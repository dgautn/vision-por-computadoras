#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Práctico 4
# visión por computadoras - 2021
# Gustavo Albarrán

import sys
import cv2
import numpy as np

if (len(sys.argv) > 1):
    filename = sys.argv[1]
else:
    print('Pasar el nombre de la imagen para recortar como argumento')
    sys.exit(0)

red = (0, 0, 255)
drawing = False     # true si el botón está presionado
buff = False        # true si hay una porción de imagen seleccionada
xybutton_down = -1, -1
recorte = -1, -1, -1, -1

try:
    imgOrig = cv2.imread(filename)      # lee la imagen pasada como parámetro 
    img = imgOrig.copy()                # crea una copia de la imagen
except:
    print('Error al abrir el archivo')
    exit(1)

def dibuja (event, x, y, flags, param):
    global xybutton_down, drawing, buff, recorte, img, imgOrig  # imgOrig no es necesario pero qda + claro
    if event == cv2.EVENT_LBUTTONDOWN and not buff:             # al presionar botón izq
        drawing = True
        xybutton_down = x, y
    elif event == cv2.EVENT_MOUSEMOVE:                          # mientras mueve el cursor
        if drawing is True:
            img = imgOrig.copy()                                # restaura la imagen original
            cv2.rectangle(img, xybutton_down, (x, y), red, 2)
    elif event == cv2.EVENT_LBUTTONUP and drawing:              # al soltar el botón
        drawing = False
        buff = True
        recorte = xybutton_down + (x, y)
        recorte = list(recorte)                                 # ordena las coordenadas del recorte
        if (recorte[0] > recorte[2]):
            recorte[0], recorte[2] = recorte[2], recorte[0]
        if (recorte[1] > recorte[3]):
            recorte[1], recorte[3] = recorte[3], recorte[1]

cv2.namedWindow('imagen')
cv2.setMouseCallback ('imagen', dibuja)

while (1):
    cv2.imshow('imagen', img)
    k = cv2.waitKey(1) & 0xFF
    if buff:                                        # si el recorte esta vacio restaura la imagen      
        if (recorte[0] == recorte[2]) or (recorte[1] == recorte[3]):
            buff = False
            img = imgOrig.copy() 
    if k == ord('r'):                                           # 'r' restaura la imagen
        buff = False
        img = imgOrig.copy() 
    elif k == ord('g') and buff:                                # 'g' guarda el recorte
        guardar = imgOrig[recorte[1]:recorte[3],recorte[0]:recorte[2],:]
        cv2.imwrite('resultado.jpg', guardar)
        break
    elif k == ord('q'):                                         # 'q' sale del programa
        break

try:                                # si puede muestra el resultado
    cv2.namedWindow('resultado')
    cv2.imshow('resultado', guardar)
    k = cv2.waitKey(0)
except:
    pass
finally:
    cv2.destroyAllWindows()
