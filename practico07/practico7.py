#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Práctico 7
# visión por computadoras - 2021
# Gustavo Albarrán

import sys
import cv2
import math
import numpy as np
import transformaciones as trf
import argparse     # recibe argumentos

########################################## FUNCIONES ##############################################

def dibuja (event, x, y, flags, param):
    global xybutton_down, drawing, buff, recorte 
    global img, imgOrig, flagAfin, contAfin, pointsAfin         # imgOrig no es necesario pero qda + claro

    if event == cv2.EVENT_LBUTTONDOWN and not buff:             # al presionar botón izq
        if flagAfin and contAfin < 3:
            pointsAfin.append((x,y))
            cv2.circle(img, (x, y), 2, red, 2)
            cv2.circle(img, (x, y), 6, blue, 2)
            contAfin += 1
            #cv2.putText(img, str(contAfin), (x, y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, blue)
        else:
            drawing = True
            xybutton_down = x, y
    
    elif event == cv2.EVENT_MOUSEMOVE:                          # mientras mueve el cursor
        if drawing is True:
            img = imgOrig.copy()                                # restaura la imagen original
            cv2.rectangle(img, xybutton_down, (x, y), red, 2)
    
    elif event == cv2.EVENT_LBUTTONUP and drawing:              # al soltar el botón
        drawing = False
        buff = True
        recorte = xybutton_down + (x, y)
        recorte = list(recorte)                                 # ordena las coordenadas del recorte
        if (recorte[0] > recorte[2]):
            recorte[0], recorte[2] = recorte[2], recorte[0]
        if (recorte[1] > recorte[3]):
            recorte[1], recorte[3] = recorte[3], recorte[1]

def check_scale (val):
    ''' 0.1 <= escala <= 10 '''
    val_fl = float(val)
    if val_fl < 0.1 or val_fl > 10:
        raise argparse.ArgumentTypeError('Use un valor entre 0.1 y 10 para la escala')
    return val_fl

def ordena ():
    '''Ordena los puntos de pointsAfin para evitar flip
    y devuelve True si son puntos colineales dentro de un margen 
    para evitar recortes muy delgados'''
    # TODO: mejorar el criterio para ordenar los puntos
    # TODO: extender a grupos de mas de 3 puntos
    global pointsAfin 

                # calcula el ángulo entre los puntos con el punto 1 como vértice
    ang = (math.atan2(pointsAfin[2][1] - pointsAfin[0][1], pointsAfin[2][0] - pointsAfin[0][0]) % (2 * math.pi) -
           math.atan2(pointsAfin[1][1] - pointsAfin[0][1], pointsAfin[1][0] - pointsAfin[0][0]) % (2 * math.pi)
           )
    
    ang = ang % (2 * math.pi)
    if ang // 0.1 == 0 or ang // 0.1 == (2 * math.pi) or ang // 0.1 == math.pi // 0.1:
        return True              # si son casi colineales retorna True de otro modo None

    elif ang > (math.pi):     # ordena los puntos
        pointsAfin[1], pointsAfin[2] = pointsAfin[2], pointsAfin[1]
    
    for n, i in enumerate(pointsAfin):  # muestra el orden
        x, y = i
        cv2.putText(img, str(n + 1), (x + 5, y - 5), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, blue)

###################################################################################################

parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description='''\
Edita una imagen con OpenCV2
----------------------------

- presione [a] y elija 3 puntos como referencia para incrustar la IMAGEN2
    puntos de referencia en la imagen para incrustar  
                        (1) +------+ (2)(3)
                            | img2 |
                            |      | 
                            +------+ (3)(2)
                                                          
- o seleccione una porción de la imagen y elija una opción
    [q] finalizar
    [r] restaura la imagen original
    [g] guardar la porción de imagen seleccionada
    [e] aplicar transformación Euclídea a la porción de imagen y guardar
    [s] aplicar transformación de similaridad a la porción de imagen y guardar
''')

parser.add_argument('filename', metavar='IMAGEN', help='Imagen para ser editada')
parser.add_argument('-o', '--output', metavar='SALIDA', help='Archivo de salida')
parser.add_argument('-i', dest='incrusta', metavar='IMAGEN2', help='Imagen para incrustar')
parser.add_argument('-a', '--angle', metavar='ANGULO', default=0.0, type=float, help='Ángulo para la transformación Euclídea')
parser.add_argument('-s', '--scale', metavar='ESCALA', default=1.0, type=check_scale, help='Escala para la transformación de similaridad')
args = parser.parse_args()

red = (0, 0, 255)
blue = (255, 0, 0)
drawing = False     # true si el botón está presionado
buff = False        # true si hay una porción de imagen seleccionada
contAfin = 0        # contador de puntos para la transformación afin
xybutton_down = -1, -1
recorte = -1, -1, -1, -1
pointsAfin = []     # lista de puntos para la transformación afin
flagAfin = False

try:
    imgOrig = cv2.imread(args.filename)     # lee la imagen pasada como parámetro 
    assert isinstance(imgOrig, np.ndarray), 'Error al abrir el archivo'
    img = imgOrig.copy()                    # crea una copia de la imagen
    if args.incrusta:
        incrusta = cv2.imread(args.incrusta)  # lee la imagen para incrustar
        assert isinstance(incrusta, np.ndarray), 'Error al abrir la imagen para incrustar'
    
    if not args.output:
        output = 'edit-' + args.filename
    else:
        output = args.output

except AssertionError as msg:
    print(msg)
    exit(1)

cv2.namedWindow('imagen')
cv2.setMouseCallback ('imagen', dibuja)

while (1):
    cv2.imshow('imagen', img)
    k = cv2.waitKey(1) & 0xFF
    
    if buff:                                                # si el recorte esta vacio restaura la imagen      
        if (recorte[0] == recorte[2]) or (recorte[1] == recorte[3]):
            buff = False
            img = imgOrig.copy() 
    
    if k == ord('r'):                                           # 'r' restaura la imagen
        buff = False
        flagAfin = False
        img = imgOrig.copy() 
    
    elif k == ord('q'):                                         # 'q' sale del programa
        break
    
    elif flagAfin:                                              # esperando para la incrustación
        if contAfin == 3:
            if ordena():                            # si son casi colineales
                flagAfin = False
                img = imgOrig.copy()
                continue
            guardar = trf.afin_incrusta(imgOrig, pointsAfin, incrusta)
            cv2.imshow('imagen', img)
            cv2.imwrite(output, guardar)
            break
    
    elif k == ord('a'):                                         # 'a' inicia incrustación
        if args.incrusta:
            flagAfin = True
            buff = False                    # anula cualquier selección
            contAfin = 0
            pointsAfin = []                 # borra la lista de puntos
            img = imgOrig.copy() 
        else:
            cv2.putText(img, 'No se seleccionó una imagen para incrustar', 
                        (0, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, red)
            cv2.imshow('imagen', img)
            cv2.waitKey(2500)
            buff = False                    # TODO:
            img = imgOrig.copy()            # Sacar fuera del if elif
    
    elif k == ord('g') and buff:                                # 'g' guarda el recorte
        guardar = imgOrig[recorte[1]:recorte[3],recorte[0]:recorte[2],:]
        cv2.imwrite(output, guardar)
        break
    
    elif k == ord('e') and buff:                            # 'e' aplica transf. Euclidea al recorte y guarda
        guardar = imgOrig[recorte[1]:recorte[3],recorte[0]:recorte[2],:]
        guardar = trf.euclid_auto(guardar, args.angle)
        cv2.imwrite(output, guardar)
        break
    
    elif k == ord('s') and buff:                            # 's' aplica transf. simil al recorte y guarda
        guardar = imgOrig[recorte[1]:recorte[3],recorte[0]:recorte[2],:]
        guardar = trf.simil_auto(guardar, args.angle, args.scale)
        cv2.imwrite(output, guardar)
        break

try:                                # si puede muestra el resultado
    cv2.namedWindow('resultado')
    cv2.imshow('resultado', guardar)
    k = cv2.waitKey(0)
except NameError:
    pass
except Exception as msg:
    print(msg)
finally:
    cv2.destroyAllWindows()
