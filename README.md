# Visión por Computadoras

## UTN FRC

**Repositorio para los trabajos prácticos de la materia Visión por computadoras de la UTN FRC**

***

Darío Gustavo Albarrán. leg. 43143

***

## Trabajos Prácticos

> Los que están tildados se encuentran listos para corregir

- [x] Práctico 1
- [x] Práctico 2
- [x] Práctico 3
- [x] Práctico 4
- [x] Práctico 5
- [x] Práctico 6
- [x] Práctico 7
- [x] Práctico 8
- [x] Práctico 9
- [ ] Práctico 10
- [x] Práctico 11
- [x] Práctico 12  [notebook en colab](https://colab.research.google.com/drive/11h9dmmFHz8JhvWaV_J6WoGRsvWKNT7o9?usp=sharing):link:
- [ ] Práctico 13
