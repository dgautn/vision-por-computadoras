NOMBRE
======

practico8 - Edita una imagen con OpenCV

SINOPSIS
========

**practico8.py** \[ **-h** \] \[ **-o** SALIDA \] \[ **-i** IMAGEN2 \]
\[ **-a** ANGULO \] \[ **-s** ESCALA \] *IMAGEN*

DESCRIPCIÓN
===========

Aplica transformaciones a la *IMAGEN* pasada como parametro obligatorio.
Se despliega una nueva ventana con la *IMAGEN* para editar.

Modo de uso:
------------

presione **\[h\]** y elija 4 puntos para aplicar rectificación
perspectiva


    (1) +---------+ (2)           (1) +-------+ (2) 
         \  img  /       ------\      |  img  | 
          \     /        ------/      |       | 
       (4) +---+ (3)              (4) +-------+ (3)

o presione **\[a\]** y elija 3 puntos como referencia para incrustar la
IMAGEN2

    (1) +------+ (2)(3) 
        | img2 | 
        |      | 
        +------+ (3)(2) 
> Fig. - puntos de referencia en la imagen para incrustar

o seleccione una porción de la imagen y elija una opción: **\[q\]**
finalizar **\[r\]** restaura la imagen original **\[g\]** guardar la
porción de imagen seleccionada **\[e\]** aplicar transformación Euclídea
a la porción de imagen y guardar **\[s\]** aplicar transformación de
similaridad a la porción de imagen y guardar

OPCIONES
========

**-h \--help**

>   Muestra el mensaje de ayuda y sale.

**-o \--output** *SALIDA*

>   Nombre para el archivo de imagen de salida. Si no se especifica se
    formatea como 'edit-' + nombre archivo de *IMAGEN*.

**-i** *IMAGEN2*

>   Archivo de imagen para incrustar aplicando la transformación Afín.
    Si no se especifica, no se admite la opción **\[a\]** en la ejecución
    del programa, mostrará advertencia y continuará permitiendo las
    demás opciones.

**-a \--angle** *ANGULO*

>   Ángulo para aplicar en la transformación Euclídea y de similaridad
    (expresado en grados sexagesimales). Si no se especifica se
    considera 0°.

**-s \--scale** *ESCALA*

>   Factor de escala para aplicar en la transformación de similaridad
    (acepta valores entre 0.1 y 10). Si no se especifica se considera 1.

ARCHIVOS
========

*transformaciones.py*

>   Módulo con las funciones Python llamadas desde **practico8.py.**

AUTOR
=====

- Gustavo Albarrán

ERRORES
=======

Por favor reportar errores.
