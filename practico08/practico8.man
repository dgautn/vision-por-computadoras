.\" manual de practico 8. Para ver en Linux
.\" man ./practico8.man
.TH "PRACTICO_8 " 1 "Junio de 2021" "CV2021 UTN FRC"
.SH NOMBRE
practico8 \- Edita una imagen con OpenCV
.SH SINOPSIS
.SY practico8.py
.OP \-h
.OP \-o SALIDA
.OP \-i IMAGEN2
.OP \-a ANGULO
.OP \-s ESCALA 
.I IMAGEN
\"\fBpractico8.py\fP [-h] [-o \fISALIDA\fP] [-i \fIIMAGEN2\fP] [-a \fIANGULO\fP] [-s \fIESCALA\fP] \fIIMAGEN\fP
.SH DESCRIPCIÓN
Aplica transformaciones a la \fIIMAGEN\fP pasada como parametro obligatorio.
Se despliega una nueva ventana con la \fIIMAGEN\fP para editar.
.SS Modo de uso:
presione \fB[h]\fP y elija 4 puntos para aplicar rectificación perspectiva
.PS

    (1) +---------+ (2)               (1) +-------+ (2)
         \\  img  /        ------\\         |  img  |
          \\     /         ------/         |       | 
       (4) +---+ (3)                  (4) +-------+ (3)

.PE
.P
o presione \fB[a]\fP y elija 3 puntos como referencia para incrustar la IMAGEN2
.PS

                     (1) +------+ (2)(3)
                         | img2 |
                         |      | 
                         +------+ (3)(2)
     Fig. - puntos de referencia en la imagen para incrustar  

.PE                                                          
.P
o seleccione una porción de la imagen y elija una opción:
   \fB[q]\fP finalizar
   \fB[r]\fP restaura la imagen original
   \fB[g]\fP guardar la porción de imagen seleccionada
   \fB[e]\fP aplicar transformación Euclídea a la porción de imagen y guardar
   \fB[s]\fP aplicar transformación de similaridad a la porción de imagen y guardar
.SH OPCIONES
.TP
\fB-h --help\fP
Muestra el mensaje de ayuda y sale.
.TP
\fB-o --output\fP \fISALIDA\fP
Nombre para el archivo de imagen de salida. Si no se especifica se formatea como 'edit-' + nombre archivo de \fIIMAGEN\fP.
.TP
\fB-i \fIIMAGEN2\fP
Archivo de imagen para incrustar aplicando la transformación Afín.
Si no se especifica, no se admite la opción \fB[a]\fP en la ejecución del programa, mostrará advertencia y continuará permitiendo las demás opciones.
.TP
\fB-a --angle\fP \fIANGULO\fP
Ángulo para aplicar en la transformación Euclídea y de similaridad (expresado en grados sexagesimales).
Si no se especifica se considera 0°.
.TP
\fB-s --scale\fP \fIESCALA\fP
Factor de escala para aplicar en la transformación de similaridad (acepta valores entre 0.1 y 10).
Si no se especifica se considera 1.
.SH ARCHIVOS
.TP
\fItransformaciones.py\fP
Módulo con las funciones Python llamadas desde \fBpractico8.py\fP.
.SH AUTOR
\[em] Gustavo Albarrán
.SH ERRORES
Por favor reportar errores.
