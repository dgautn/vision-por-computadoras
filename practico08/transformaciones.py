#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2021, Gustavo AlbarrAn
#
# Se agregan funciones para práctico 8

""" Transformaciones con OpenCV para VisiOn por Computadoras -- v4.0 -- """

import cv2
import numpy
from typing import List     # alias de list para recomendaciones de tipo

def euclid (img, angle, tx, ty, center=None):
    """Transformación Euclidia - traslación + rotación isométrica"""

    scale = 1   # euclidea -> isometrica
    h, w = img.shape[:2]                                # dimensiones de la imagen

    if center is None:                                  # centro de rotación
        center = (w // 2, h // 2)                       # sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,scale)   # matriz de rotación

    M[:, 2] += [tx, ty]                                 # agrega la traslación
    
    return cv2.warpAffine(img, M, (w, h))    


def euclid_auto (img, angle):
    """Transformación Euclidia con autoajuste.
    
    Centro de rotación sobre centro de la imagen, ajuste automático de traslación 
    y dimensión de imagen de salida para que no quede recortada.
    """
    # sobre un código de Adrian Rosebrock
    # https://github.com/jrosebr1/imutils
    
    scale = 1                                           # euclidea -> isometrica
    h, w = img.shape[:2]                                # dimensiones de la imagen

    center = (w // 2, h // 2)                           # centro de rotación sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,scale)   # matriz de rotación

    cos = numpy.abs(M[0, 0])
    sin = numpy.abs(M[0, 1])

    nW = int((h * sin) + (w * cos))                     # calcula dimensiones para imagen de salida
    nH = int((h * cos) + (w * sin))

    M[:, 2] += [(nW - w) // 2, (nH - h) // 2]           # calcula traslación para llevar al centro de
                                                        # la imagen de salida

    return cv2.warpAffine(img, M, (nW, nH))    



def simil (img, angle, tx, ty, s, center=None):
    """Transformación de similaridad - Euclidia + escalado"""

    h, w = img.shape[:2]                                # dimensiones de la imagen

    if center is None:                                  # centro de rotación
        center = (w // 2, h // 2)                       # sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,s)       # matriz de rotación

    M[:, 2] += [tx, ty]                                 # agrega la traslación
    
    return cv2.warpAffine(img, M, (w, h))    


def simil_auto (img, angle, s):
    """Transformación de similaridad con autoajuste.
    
    Centro de rotación sobre centro de la imagen, ajuste automático de traslación 
    y dimensión de imagen de salida para que no quede recortada.
    """
    # sobre un código de Adrian Rosebrock
    # https://github.com/jrosebr1/imutils
    
    h, w = img.shape[:2]                                # dimensiones de la imagen

    center = (w // 2, h // 2)                           # centro de rotación sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,s)       # matriz de rotación

    cos = numpy.abs(M[0, 0])
    sin = numpy.abs(M[0, 1])

    nW = int((h * sin) + (w * cos))                     # calcula dimensiones para imagen de salida
    nH = int((h * cos) + (w * sin))

    M[:, 2] += [(nW - w) // 2, (nH - h) // 2]           # calcula traslación para llevar al centro de
                                                        # la imagen de salida

    return cv2.warpAffine(img, M, (nW, nH))    

def afin (img: numpy.ndarray, pts1: List, pts2: List) -> numpy.ndarray:
    '''Transformación Afín

    Nota: Puede recibir dos listas con tres puntos en pares ordenados 
    que luego son convertidos a numpy array de float32
    o directamente dos numpy.ndarray.float32
    '''
    h, w = img.shape[:2]                                # dimensiones de la imagen
    pts1 = numpy.float32(pts1)
    pts2 = numpy.float32(pts2)
    M = cv2.getAffineTransform(pts1, pts2)
    return cv2.warpAffine(img, M, (w, h))

def afin_incrusta (img1: numpy.ndarray, ptsImg1: List, img2: numpy.ndarray) -> numpy.ndarray:
    '''Incrusta imagen con transformación Afín
    
    pega la imagen 2 en los puntos pasados de la imagen 1
    '''
    h1, w1 = img1.shape[:2]                              # dimensiones de la imagen
    h2, w2 = img2.shape[:2]                              # dimensiones de la imagen2
    pts1 = numpy.float32([[0, 0],                   #
                          [w2, 0],                  # esquinas de la imagen 2
                          [w2, h2]])                # 
    pts2 = numpy.float32(ptsImg1)
    unos = numpy.ones((h2, w2, 3))                      # matriz de unos con tamaño de img2
    M = cv2.getAffineTransform(pts1, pts2)

    recorte = cv2.warpAffine(img2, M, (w1, h1))     # transforma img2 con tamaño de salida de img1
    mascara = cv2.warpAffine(unos, M, (w1, h1))     # parche tamaño y forma de incrustación
    img1[mascara == 1] = 0                      # recorta la imagen 1 
    return img1 + recorte                       # junta las imagenes

def perspect (img: numpy.ndarray, pts1: List, pts2: List = None) -> numpy.ndarray:
    '''Rectifica la perspectiva de la imagen

    Nota: Puede recibir dos listas con cuatro puntos en pares ordenados 
    que luego son convertidos a numpy array de float32
    o directamente dos numpy.ndarray.float32
    '''    
    pts1 = numpy.float32(pts1)                  # convierte a float32
    if pts2:
        pts2 = numpy.float32(pts2)              # si se recicibe parametro
    else:
        xx = max(pts1[:, 0]) - min(pts1[:, 0])              # limites según tamaño del recorte
        yy = max(pts1[:, 1]) - min(pts1[:, 1])
        pts2 = numpy.float32([[0 , 0 ],                 #
                              [xx, 0 ],                 # esquinas de un rectángulo 
                              [xx, yy],                 # sin rotación para la imagen
                              [0 , yy]])                # 

    M = cv2.getPerspectiveTransform(pts1, pts2)     # matriz para la rectificación

    xout = max(pts2[:, 0]) - min(pts2[:, 0])        # ancho y alto para imagen de salida
    yout = max(pts2[:, 1]) - min(pts2[:, 1])        # se calcula de nuevo por si fueron enviados como parámetro

    #print (type(xout))
    try:
        #raise Exception
        xout = int(xout)
        yout = int(yout)
    except:             ## No quiero que falle mas (default gateway)
        yout, xout = img.shape[:2]

    return cv2.warpPerspective(img, M, (xout, yout)) 
