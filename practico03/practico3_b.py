#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Práctico 3 - parte a
# visión por computadoras - 2021
# Gustavo Albarrán

import cv2

cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
ancho = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)) # Devuelve el ancho del video (float -> entero)
alto = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) # Devuelve el alto del video (float -> entero)
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (ancho, alto))

while (cap.isOpened()):
    ret, frame = cap.read()
    if ret is True:
        out.write(frame)
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
