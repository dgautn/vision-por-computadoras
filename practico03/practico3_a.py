#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Práctico 3 - parte a
# visión por computadoras - 2021
# Gustavo Albarrán

import sys
import cv2

if (len(sys.argv) > 1):
    filename = sys.argv[1]
else:
    print('Pasar el nombre del video como argumento')
    sys.exit(0)

cap = cv2.VideoCapture(filename)
fps = int(cap.get(cv2.CAP_PROP_FPS)) # Devuelve un float con los fps (float -> entero)
wt = 1000//fps # espera entre frames en ms (division entera)

while (cap.isOpened()):
    ret, frame = cap.read()
    if ret:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('frame', gray)
        if ((cv2.waitKey(wt) & 0xFF) == ord('q')):
            break
    else:
        break
    
cap.release()
cv2.destroyAllWindows()
