#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2021, Gustavo AlbarrAn

""" Transformaciones con OpenCV para VisiOn por Computadoras -- v2.0 -- """

import cv2
import numpy

def euclid(img, angle, tx, ty, center=None):
    """Transformación Euclidia - traslación + rotación isométrica"""

    scale = 1   # euclidea -> isometrica
    h, w = img.shape[:2]                                # dimensiones de la imagen

    if center is None:                                  # centro de rotación
        center = (w // 2, h // 2)                       # sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,scale)   # matriz de rotación

    M[:, 2] += [tx, ty]                                 # agrega la traslación
    
    return cv2.warpAffine(img, M, (w, h))    


def euclid_auto(img, angle):
    """Transformación Euclidia con autoajuste.
    
    Centro de rotación sobre centro de la imagen, ajuste automático de traslación 
    y dimensión de imagen de salida para que no quede recortada.
    """
    # sobre un código de Adrian Rosebrock
    # https://github.com/jrosebr1/imutils
    
    scale = 1                                           # euclidea -> isometrica
    h, w = img.shape[:2]                                # dimensiones de la imagen

    center = (w // 2, h // 2)                           # centro de rotación sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,scale)   # matriz de rotación

    cos = numpy.abs(M[0, 0])
    sin = numpy.abs(M[0, 1])

    nW = int((h * sin) + (w * cos))                     # calcula dimensiones para imagen de salida
    nH = int((h * cos) + (w * sin))

    M[:, 2] += [(nW - w) // 2, (nH - h) // 2]           # calcula traslación para llevar al centro de
                                                        # la imagen de salida

    return cv2.warpAffine(img, M, (nW, nH))    



def simil(img, angle, tx, ty, s, center=None):
    """Transformación de similaridad - Euclidia + escalado"""

    h, w = img.shape[:2]                                # dimensiones de la imagen

    if center is None:                                  # centro de rotación
        center = (w // 2, h // 2)                       # sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,s)       # matriz de rotación

    M[:, 2] += [tx, ty]                                 # agrega la traslación
    
    return cv2.warpAffine(img, M, (w, h))    


def simil_auto(img, angle, s):
    """Transformación de similaridad con autoajuste.
    
    Centro de rotación sobre centro de la imagen, ajuste automático de traslación 
    y dimensión de imagen de salida para que no quede recortada.
    """
    # sobre un código de Adrian Rosebrock
    # https://github.com/jrosebr1/imutils
    
    h, w = img.shape[:2]                                # dimensiones de la imagen

    center = (w // 2, h // 2)                           # centro de rotación sobre el centro de imagen

    M = cv2.getRotationMatrix2D(center, angle ,s)       # matriz de rotación

    cos = numpy.abs(M[0, 0])
    sin = numpy.abs(M[0, 1])

    nW = int((h * sin) + (w * cos))                     # calcula dimensiones para imagen de salida
    nH = int((h * cos) + (w * sin))

    M[:, 2] += [(nW - w) // 2, (nH - h) // 2]           # calcula traslación para llevar al centro de
                                                        # la imagen de salida

    return cv2.warpAffine(img, M, (nW, nH))    


