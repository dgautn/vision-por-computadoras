#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Práctico 1
# visión por computadoras - 2021
# Gustavo Albarrán

import random 

def adivinar(intentos):
    numero = random.randint(0, 100) # genera un numero aleatorio  entre 0 y 100
    numIntentos = 1
    # una ayudita para el debugeo
    print('\tpssssstt... el numero es', numero, '... No digas que te dije.....\n\n')
    while intentos:
        try: 
            # solicita un numero e intenta convertirlo a entero
            elecc = int(input('Adivina el numero: '))
            # verifica el rango
            assert elecc >= 0 and elecc <= 100 
            if elecc == numero:
                print('\tAdivinaste el numero en', numIntentos, 'intento' if numIntentos == 1 else 'intentos')
                break # termina el while sin pasar por el else (del while)
        except AssertionError: # si el numero no esta dentro del rango
            print('\tdebe ser un numero entre 0 y 100!!')
        except: # aca llega si no pudo convertir a entero
            print('\tentrada no valida')
        else: # si no selevanto ninguna excepcion
            print('\tNo, no es ese')
            intentos -= 1
            numIntentos +=1
    else: # Aca llega solo si la condicion del while se hizo "False"
        print('\tSuperaste la cantidad de intentos permitida')
    return None

adivinar(5)
