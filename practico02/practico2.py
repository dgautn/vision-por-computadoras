#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Práctico 2
# visión por computadoras - 2021
# Gustavo Albarrán

import cv2

img = cv2.imread('hoja.jpg', 0) # 0 = cv2.IMREAD_GRAYSCALE

for row in range(len(img)):
    for col in range(len(img[row])):
        img[row][col] = 0 if img[row][col] < 200 else 255

cv2.imwrite('resultado.jpg', img)
