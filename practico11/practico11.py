#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Práctico 11
# Visión por Computadoras - 2021
# Gustavo Albarrán

import numpy as np
import cv2
import argparse     # recibe argumentos

MIN_MATCH_COUNT = 10

parser = argparse.ArgumentParser(description='Alineación de imágenes usando SIFT')

parser.add_argument('filename1', metavar='IMAGEN1', help='Imagen para ser editada')
parser.add_argument('filename2', metavar='IMAGEN2', help='Imagen para ser editada')
args = parser.parse_args()

try:
    img1 = cv2.imread(args.filename1)    # Leemos la imagen 1
    img2 = cv2.imread(args.filename2)    # Leemos la imagen 2

    assert isinstance(img1, np.ndarray), 'Error al abrir el archivo'
    assert isinstance(img2, np.ndarray), 'Error al abrir el archivo'

    img1_gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    h, w = img1.shape[:-1]

except AssertionError as msg:
    print(msg)
    exit(1)

dscr = cv2.xfeatures2d.SIFT_create()    # Inicializamos el detector y el descriptor

kp1, des1 = dscr.detectAndCompute(img1_gray, None)  # Encontramos los puntos clave y los descriptores con SIFT en la imagen 1
kp2, des2 = dscr.detectAndCompute(img2_gray, None)  # Encontramos los puntos clave y los descriptores con SIFT en la imagen 2

matcher = cv2.BFMatcher(cv2.NORM_L2)

matches = matcher.knnMatch(des1, des2, k =2)

# Guardamos los buenos matches usando el test de razón de Lowe
good = []
for m, n in matches:
    if m.distance < 0.7 * n.distance:
        good.append(m)

if (len(good) > MIN_MATCH_COUNT):
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1, 1, 2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1, 1, 2)

    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0) # Computamos la homografía con RANSAC

wimg2 = cv2.warpPerspective(img2, H, (w, h))    # Aplicamos la transformación perspectiva H a la imagen 2

# Mezclamos ambas imágenes
alpha = 0.75 #0.5
blend = np.array(wimg2 * alpha + img1 * (1 - alpha), dtype=np.uint8)

# cv2.imwrite('img_alineadas.png', blend)

cv2.imshow('img', blend)
cv2.waitKey()
