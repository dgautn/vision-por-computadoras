#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Práctico 9
# visión por computadoras - 2021
# Gustavo Albarrán

import sys
import cv2
import math
import numpy as np
#import transformaciones as trf
import argparse                 # recibe argumentos
import tkinter as tk
from typing import List, Tuple  # alias de list y tuple para recomendaciones de tipo

class Popup:
    '''Ventana emergente para ingresar un valor 
    el valor se asigna a la variable pública dst
    recibe parámetro msg para título de ventana'''
    def __init__(self, msg):
        self.__pop = tk.Tk()
        self.__pop.title('')
        self.__pop.geometry('180x75')
                # casilla de entrada
        self.__entry = tk.Entry(self.__pop, width=8, justify=tk.RIGHT)
        self.__entry.place(x=50, y=5)
        self.__entry.bind('<Return>', self.__getValor)          # tecla Enter
        self.__entry.bind('<KP_Enter>', self.__getValor)        # tecla Enter Keypad
        self.__entry.focus_set()
                # etiqueta izq
        self.__labelL = tk.Label(self.__pop, text=msg)
        self.__labelL.place(x=5, y=5)
                # etiqueta der
        self.__labelR = tk.Label(self.__pop, text='metros')
        self.__labelR.place(x=125, y=5)
                # botón
        self.__button = tk.Button(self.__pop, text="OK", command=self.__getValor) # boton 'OK'
        self.__button.place(x=70, y=35)

        self.__pop.mainloop()

    def __getValor(self, __event = None):   # asigna valor a variable pública y cierra ventana
        self.dst = self.__entry.get()
        self.__pop.destroy()

########################################## FUNCIONES ##############################################

def dibuja (event, x, y, flags, param):
    '''Callback para la ventana con la imagen original'''
    global drawing, img, imgOrig, points

    if event == cv2.EVENT_LBUTTONDOWN:            # al presionar botón izq
        if len(points) < 4:
            points.append((x,y))
            drawing = True
    
    elif event == cv2.EVENT_MOUSEMOVE:                          # mientras mueve el cursor
        if drawing is True and len(points) < 4:
            img = imgOrig.copy()                                # restaura la imagen original
            for i in range(1, len(points)):
                cv2.line(img, points[i-1], points[i], green, 2)     #
            if len(points):                                         #
                cv2.line(img, points[-1], (x, y), green, 2)         # dibuja lineas contorno 
            if len(points) == 3:                                    #
                cv2.line(img, points[0], (x, y), green, 2)          #
    
def medir (event, x, y, flags, param):
    '''Callback para la pantalla de mediciones'''
    global imgRect, imgRectOrig, buffMed, dstPoint, rel

    if event == cv2.EVENT_LBUTTONDOWN:            # al presionar botón izq
        if not buffMed:
            dstPoint = (x, y)
            buffMed = True
        elif buffMed:
            buffMed = False

    elif event == cv2.EVENT_MOUSEMOVE:                          # mientras mueve el cursor
        if buffMed:
            imgRect = imgRectOrig.copy()                        # restaura la imagen original
            cv2.circle(imgRect, dstPoint, 5, blue, 2)
            cv2.circle(imgRect, (x, y), 5, blue, 2)
            cv2.line(imgRect, dstPoint, (x, y), green, 2)            #
            dif = np.sqrt(((dstPoint[0] - x) ** 2) + ((dstPoint[1] - y) ** 2))
            dif = dif * rel
            cv2.putText(imgRect, '{:.2f}m'.format(dif), (x + 5, y - 5), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, blue)

def dibujaPuntos(puntos: List) -> None:
    '''Redibuja el poligono en gris
    e indica los vertices ordenados'''
    for i in range(1, 4):                   ## dibuja las lineas en gris
        cv2.line(img, puntos[i-1], puntos[i], gray, 2)
    else:
        cv2.line(img, puntos[0], puntos[i], gray, 2)
    
    ordPoints = ['a', 'b', 'c', 'd']
    for punto in puntos:                    ## indica los vertices
        x, y = punto
        cv2.circle(img, punto, 6, blue, 2)
        ordPoint = ordPoints.pop(0)
        cv2.putText(img, ordPoint, (x + 5, y - 5), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, blue)

def ordena (puntos: List) -> List:
    '''Ordena los puntos en sentido horario y desde el más 
    cercano al más lejano del origen de coordenadas'''

                # calcula el ángulo entre los puntos 2 y 3 con el punto 1 como vértice
    ang = (math.atan2(puntos[2][1] - puntos[0][1], puntos[2][0] - puntos[0][0]) % (2 * math.pi) -
           math.atan2(puntos[1][1] - puntos[0][1], puntos[1][0] - puntos[0][0]) % (2 * math.pi)
           )
    
    ang = ang % (2 * math.pi)

    if ang > (math.pi):     # si se recorren en antihorario ordena los puntos
        puntos = puntos[::-1]
    
    dist = []
    for punto in puntos:
        x, y = punto
        dist.append( np.sqrt((x ** 2) + (y ** 2)) ) # hipotenusa desde el origen
    minim = dist.index(min(dist))           # indice del punto mas cerca del origen
    npuntos = (puntos * 2)[minim:minim + len(puntos)] # concatena x 2 y corta desde el minimo 
    return npuntos

def cuadroTexto(img, texto, origen: Tuple) -> None:
    '''Imprime cuadro de texto'''
    x, y = origen
    lineas = texto.splitlines()
    maxlen = max(len(lin) for lin in lineas)    # len de la linea mas larga 
    cv2.rectangle(img, origen, (x + 20 + maxlen * 10, y + 20 + 15 * len(lineas)), (0, 0, 0), -1)
    cv2.rectangle(img, origen, (x + 20 + maxlen * 10, y + 20 + 15 * len(lineas)), green, 2)
    i = y + 20
    for linea in lineas: # imprime cada linea de texto
        cv2.putText(
                    img, 
                    linea,
                    (x + 15, i), 
                    cv2.FONT_HERSHEY_COMPLEX_SMALL, 
                    0.72, 
                    green)
        i += 15

###################################################################################################

parser = argparse.ArgumentParser(description='''\
        El programa rectifica una imagen a partir de las dimensiones conocidas de un rectangulo 
        en el sistema original, y luego permite tomar medidas en la imagen redimensionada
        ''')

parser.add_argument('filename', metavar='IMAGEN', help='Imagen para ser procesada')
parser.add_argument('-o', '--output', metavar='SALIDA', help='Archivo de salida')
args = parser.parse_args()

red = (0, 0, 255)
blue = (255, 0, 0)
green = (0, 255, 0)
gray = (100, 100, 100)
drawing = False     # true si el botón está presionado
points = []         # lista de puntos para la transformación perspectiva

try:
    imgOrig = cv2.imread(args.filename)     # lee la imagen pasada como parámetro 
    assert isinstance(imgOrig, np.ndarray), 'Error al abrir el archivo'
    img = imgOrig.copy()                    # crea una copia de la imagen
    
    if not args.output:
        output = 'edit-' + args.filename
    else:
        output = args.output

except AssertionError as msg:
    print(msg)
    exit(1)

cv2.namedWindow('Imagen')
cv2.setMouseCallback ('Imagen', dibuja)

cuadroTexto(img, 
            '''Seleccione cuatro vertices de un rectangulo\nde dimensiones conocidas en la escena''', 
            (25, 25) ) 

cuadroTexto(img, 
            '''[q] Salir\n[r] Restaurar''', 
            (25, 80) ) 

while (1):
    cv2.imshow('Imagen', img)
    k = cv2.waitKey(1) & 0xFF
    
    if k == ord('r'):                                           # 'r' restaura la imagen
        img = imgOrig.copy() 
        points = []
        drawing = False

    elif k == ord('q'):                                         # 'q' sale del programa
        exit(0)
        #break

    elif len(points) == 4:                                      # si ya tiene los 4 puntos
    # TODO: llevar a funciones ... o modulo ??
        img = imgOrig.copy() 

        points = ordena(points)

        dibujaPuntos(points)

        cv2.line(img, points[0], points[1], blue, 2)     #
        cv2.imshow('Imagen', img)
        cv2.waitKey(100)
        ab = Popup('[a-b]')         ## popup solicita distancia a-b

        cv2.line(img, points[1], points[2], blue, 2)     #
        cv2.line(img, points[0], points[1], gray, 2)     #
        cv2.imshow('Imagen', img)
        cv2.waitKey(100)
        bc = Popup('[b-c]')         ## popup solicita distancia b-c

        try:                        # Si no puede pasar a float
            abDst = float(ab.dst)   # limpia todo y vuelve al bucle
            bcDst = float(bc.dst)   # 
        except:                     # 
            img = imgOrig.copy()    # 
            points = []             # 
            drawing = False         # 
            continue                # 

                            # distancia ab en la imagen
        abImg = np.sqrt(((points[0][0] - points[1][0]) ** 2) + ((points[0][1] - points[1][1]) ** 2))
                            # proporcion bc
        bcImg = bcDst * abImg / abDst

        pointsDest = np.float32(((0    , 0    ),  # Rectangulo con relación de aspecto conocida
                                 (abImg, 0    ),
                                 (abImg, bcImg),
                                 (0    , bcImg) ))
        points = np.float32(points)
        
        M = cv2.getPerspectiveTransform(points, pointsDest)     # Matriz de transformación perspectiva

        ############################################################### 
        # NOTE: En esta parte se trata de escalar y trasladar la imagen 
        # rectificado para que se vea toda la imagen original
        # TODO: bug con algunas imagenes - descarga.jpeg
        ###############################################################
        h, w = img.shape[:2]                                    # dimensiones de la imagen
        
        corners = np.float32(((0    , 0    ),                   # esquinas de la imagen original
                              (0    , h - 1),
                              (w - 1, h - 1),
                              (w - 1, 0    ) ))

        corners = np.float32([corners])         # es necesario para adaptar el formato del array ???

                   # calcula donde van a parar los vertices originales con la transformación
        cornersNew = cv2.perspectiveTransform(corners, M)

        bx, by, bwidth, bheight = cv2.boundingRect(cornersNew)  # Rectangulo mínimo
                   # Matriz de traslación para ubicar la imagen transformada en el plano visible
        A = np.float32(((1, 0, -bx),
                        (0, 1, -by),
                        (0, 0,   1) ))
        M = A.dot(M)                            # Junta las 2 transformaciones en una sola matriz
        ###############################################################

        imgRect = cv2.warpPerspective(imgOrig, M, (bwidth, bheight)) # aplica las transformaciones

                    # Ajusta al tamaño original   nueva relación = w / bwidth
        rel = (bwidth / w) * (abDst / abImg)
       #w = bwidth * w / bwidth  
        h = int( round(bheight * w / bwidth))
        imgRect = cv2.resize(imgRect, (w, h))
        break

cv2.destroyWindow('Imagen')
cv2.namedWindow('Medidas')
cv2.setMouseCallback ('Medidas', medir)
buffMed = False
imgRectOrig = imgRect.copy()

cuadroTexto(imgRect, 
            '''Marque dos puntos para conocer sus medidas''', 
            (25, 25) ) 

cuadroTexto(imgRect, 
            '''[q] Salir\n[r] Restaurar\n[g] Guardar''', 
            (25, 60) ) 

while (1):
    cv2.imshow('Medidas', imgRect)
    k = cv2.waitKey(1) & 0xFF

    if k == ord('q'):                   # 'q' sale del programa
        break

    elif k == ord('r'):                 # 'r' restaura la imagen
        imgRect = imgRectOrig.copy() 
        buffMed = False

    elif k == ord('g'):                 # 'g' guarda el recorte
        cv2.imwrite(output, imgRect)
        break

cv2.destroyAllWindows()
